**Short biography of the Robert Lewis Stevenson**

The name of Robert Lewis Stevenson from childhood is familiar to everyone who can not imagine life without a book. The incredible and exciting adventures that await the heroes of his works at every turn, more than once forced readers to sit for hours on the pages of Treasure Island and Black Arrow. And although these works are considered the most famous in the bibliography of the writer, the list of books by Stevenson is not limited to them.

Stevenson Robert Lewis (1850-1894) - English writer, Scot by birth, literary critic and [papersowl review](https://topessaycompanies.com/best-essay-services/papersowl-com/), poet, founder and theorist of neo-romanticism. Stevenson was born in Edinburgh (Scotland) in the family of a lighthouse civil engineer. I’ve been sick a lot since childhood. It seemed that he did not have the opportunity to become the author of one of the most famous adventure novels in the future, since he had extremely many diseases.

**Childhood and youth**

The boy’s father owned an unusual profession - he was an engineer designing lighthouses. From early childhood, the boy lay in bed for a long time - serious diagnoses made his parents take care of his son. Stevenson was diagnosed with croup, and later consumption, (pulmonary tuberculosis), which often became fatal in those days. Therefore, little Robert spent a lot of time in the "blanket country" - so the writer later writes about his childhood.

Perhaps the constant restrictions and bed rest helped Robert Lewis Stevenson's imagination to develop so much that he began to invent imaginary adventures and travels that he could not make in life. In addition, the boy’s nanny instilled in him a literary taste and sense of speech, reading poems by Robert Burns and telling bedtime stories.

At the age of 15, Robert Lewis Stevenson finished the first serious work, called the Pentland Uprising. Robert's father supported his son and published this book in 100 copies for his own money in 1866.

Around the same time, Stevenson, despite his state of health, began to travel to his native Scotland and Europe and record impressions and cases from trips. Later, these essays came out under the cover of the book “Roads” and “Traveling Inland”.

**Creativity and work of Robert**

The first serious work of Stevenson, which brought fame to the writer, was a story called "Overnight by Francois Villon." And already in 1878, the prose writer, while on a regular trip to France, ends the cycle of stories that came out as a whole. This collection was called “Suicide Club” and later became one of Stevenson’s most famous works. The Suicide Club, as well as the Almaz Raji series of stories, have been published in many literary magazines in Europe. Gradually, Stevenson's name became recognizable.

However, the writer recognized serious fame in 1883, when perhaps Stevenson’s best novel, [Treasure Island](https://search.engadget.com/search;_ylc=X3IDMgRncHJpZANlQkVnT0xZblRSbVVUdE92R1I0blpBBG5fc3VnZwM5BHBvcwMwBHBxc3RyAwRwcXN0cmwDMARxc3RybAMxNQRxdWVyeQNUcmVhc3VyZSUyMElzbGFuZAR0X3N0bXADMTU4NjcxMTY2MA--?p=Treasure+Island&fr=engadget), was published. Like many brilliant works, this book began with humorous stories with which Stephenson entertained his little stepson. Robert Lewis even drew for the boy a map of the invented island, which was printed almost unchanged in the preface to the publication. The Black Arrow follows from the writer’s pen, in 1885, Prince Otto and the cult novel The Strange Story of Dr. Jekyll and Mr. Hyde. " A year later, Robert Lewis Stevenson finished work on the next storybook, entitled "And Another New Thousand and One Nights" (or "Dynamite").

**Personal life and last years of life**

While traveling in [France](https://www.engadget.com/france-google-scraping-stories-snippets-104038211.html), Robert Lewis Stevenson met Francis Matilda Osbourne. Fanny - as Stephenson affectionately called her lover - was married. In addition, the woman had two children and she was 10 years older than Stevenson. It seemed that this could prevent the lovers from being together.

At first, this happened - Stevenson left France alone, without a lover, mourning for his failed personal life. But in 1880, Fanny finally managed to divorce her husband and marry a writer who became an overnight happy husband and father. The couple did not have common children.

The island of Samoa has become not only the writer's favorite place, but also the last refuge. December 3, 1894 Robert Lewis Stevenson died. In the evening, the man, as usual, went down to dinner, but suddenly grabbed his head, struck by a blow. A few hours later the writer was no longer alive. The cause of the genius's death was a stroke. There, on the island, the writer's grave is still preserved. The natives, truly saddened by the death of their hero and the “storytelling leader,” buried Robert Lewis Stevenson on the top of a mountain called Weah, placing a tombstone of concrete on the grave.